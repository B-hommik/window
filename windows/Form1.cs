﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace windows
{
    public partial class Form1 : Form

    {
        static readonly string filename = @"..\..\inimesed.txt";
        List<Inimene> Inimesed = new List<Inimene>
        {new Inimene { Nimi="Thea", Vanus = 33},
        new Inimene {Nimi ="Mart", Vanus = 34},
        new Inimene { Nimi ="Krister", Vanus = 14},

        };
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Elu on lill";
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.label2.Text = "Ära nuta lillekapsas";
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            string nimi = NameBox.Text; //kontrollime et nimi liiga lühike poleks
            if (nimi.Length < 2)
            {
                ErrorLabel.Text += "Nimi liiga lühike\n"; return; //return tähendab et lõpetame asja ära, ei lähe edasi, meetodi break.
            }
            int vanus;
            if (!int.TryParse(AgeBox.Text, out vanus))//proovime kas vanust saab intiks teha, kui õnnestub siis paneb out parameetri sisse arvu. Kui ei õnnestu, siis ta ütleb meie lause.
            { ErrorLabel.Text += "Vanus on vigane\n"; return; }

            //if (int.TryParse(AgeBox.Text, out vanus)); else { ErrorLabel.Text = "Vanus on vigane"; return; } VÕIB KA NII KIRJUTADA
            if (vanus < 18)
            { ErrorLabel.Text += "Liiga noor\n"; return; }
            if (vanus > 120)
            { ErrorLabel.Text += "Liiga vana\n"; return; }

            try
            {

                Inimene uus = new Inimene
                {
                    Nimi = NameBox.Text,
                    Vanus = int.Parse(AgeBox.Text)
                };
                Inimesed.Add(uus);
                NameBox.Text = "";
                AgeBox.Text = "";
                ErrorLabel.Text = "";
            }
            catch (Exception err)
            {
                ErrorLabel.Text = err.Message;
            }

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Inimesed;


        }

        private void ListButton_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = Inimesed;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            File.WriteAllLines(filename,
                Inimesed.Select(x => $"{x.Nimi},{x.Vanus}")); //teeme inimesest stringi, et saaksime faili kirjutada.

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Inimesed =
                File.ReadAllLines(filename)
                .Select(x => x.Split(','))
                .Select(x => new Inimene { Nimi = x[0], Vanus = int.Parse(x[1]) })
                .ToList();
            dataGridView1.DataSource = null; // see on nagu restart, et ta uuesti inimesi hakkaks näitama
            dataGridView1.DataSource = Inimesed;
        }

        private void JsonButton_Click(object sender, EventArgs e)
        {
            string jsonFilename = @"..\..\inimesed.json";
            string json =
                JsonConvert.SerializeObject(Inimesed);
            File.WriteAllText(jsonFilename, json);
        }

        private void JsonShowButton_Click(object sender, EventArgs e)
        {
            string jsonFilename = @"..\..\inimesed.json";
            Inimesed =
                JsonConvert.DeserializeObject<List<Inimene>>(File.ReadAllText(jsonFilename));
            dataGridView1.DataSource = null; // see on nagu restart, et ta uuesti inimesi hakkaks näitama
            dataGridView1.DataSource = Inimesed;

        }
    }
}
